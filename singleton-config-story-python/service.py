import sys

class EchoService:
    def __init__(self, target=None) -> None:
        self.target = target or sys.stdout
    
    def echo(self, text: str):
        print(text, file=self.target)
