# Singleton config story
**To read**: [<link to story opened in Refactories Story UI, just link from browser is good enough>]

**Estimated reading time**: <estimate, change later after feedback>

## Story Outline
This story shows how any application with a complex configuration structure can be optimised using the Singleton pattern.

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_code #python #patterns
