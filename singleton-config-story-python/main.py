from .service import EchoService

def main():
    echo_service = EchoService()
    
    echo_service.echo("Hello world!")


if __name__ == "__main__":
    main()
